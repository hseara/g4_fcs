static char *SRCID_template_c = "$Id$";

#include <math.h>
#include <string.h>
#include <gromacs/macros.h>
#include <gromacs/statutil.h>
#include <gromacs/index.h>
#include <gromacs/tpxio.h>
#include <gromacs/smalloc.h>
#include <gromacs/copyrite.h>
#include <gromacs/pbc.h>
#include <gromacs/rmpbc.h>
#include <gromacs/typedefs.h>
#include <gromacs/gmx_fatal.h>
#include <gromacs/vec.h>
#include <gromacs/xvgr.h>
#include <fftw3.h>              //for the fourirer transforms

static int debug_level=0;



//NOTE:
/* During this program we will use the following notation:
 * posid = index[iD]
 * posid = absolut position in trajectory file
 * id = position in the index file
 * If several index files are possible a note is required in deinition.
 */

/* ################################
 *         fcs_struct
 * ################################ */

/*! @struct fcs_struct
 *  @brief This structure contains all information required and gathered
     during the fcs calculation.
 *
 *  @var fcs_struct::axis_fcs
 *  FCS dimension: 0=yz-plane, 1=xz-plane, 2=xy-planeNumber and 9=3d.
 *  @var fcs_struct::n_pbc_displ
 *  Number of periodic boxes around the central one to be considered.
 *  Important for the fake PBC option.
 *  @var fcs_struct::pbcboxes
 *  Number of pbc boxes. (1+2*n_pbc_images)^(DIM), from pbc_displ.
 *
 *  @var fcs_struct::ntrj
 *  Number of trajectories to be used in the fcs. Depends on the number of
 *  dyes, their number per pbcbox, and number of pbcboxes per trajectory.
 *
 *  @var fcs_struct:trj
 *  Pointer to the data of each trajectory used
 *
 */
typedef struct fcs_struct {
  //Geometry parametes FCS analysis
  int            axis_fcs;
  int            n_pbc_displ;
  int            pbcboxes;

  //Description trajectories
  int            n_trjs;
  struct fcstrj *trj;

  //Parameters fcs experiment
  int type_fcs;
  real focus;

  //Output fcs experiment
  double  *acf_intensity_ensemble;
} fcs_struct;


/*! @struct fcstrj
 *  @brief This structure monitor all information about selected dyes.
 *
 *  @var fcstrj::isize_index
 *  Numer of atoms in the index file
 *  @var fcstrj::index
 *  Pointer to the index file containing all atoms of possible dyes
 *  @var fcstrj::n_dyes_index
 *  Number of possible dyes in the index file
 *  @var fcstrj::n_atoms_dyes_index
 *  Number of atoms in each dye of the index file
 *
 *  @var fcstrj::n_dyes
 *  Current Number of dyes in the considerd trajectory
 *  @var fcstrj::id_dyes
 *  Array with "id" of the dyes. Number of appearance in input index file.
 *  @var fcstrj::pbcbox_dyes
 *  Pointer containing the box number where every dye is placed
 *  @var fcstrj::n_atoms_dyes
 *  Array with number of atoms in each dye.
 *  @var fcstrj::id_atoms_dyes
 *  Array for each dye containing the indexes for every atoms in the index file.
 *  @var fcstrj::pos_atoms_dyes
 *  Array for each dye containing the position every atoms as in trajectory file.
 *  @var fcstrj::xyz_at0_dyes
 *  Coordinates of the first atom of every dye in a pbc box. Used to check if the
 *  dye jumps acrross pbc. Updated every frame.
 *
 */
struct fcstrj {
  //Parameters of input index file used for this group(Trajectory).
  //Note: They can be the same between groups(FCS Trajectories)
  // but each has their own copy/pointer
  int            isize_index;
  atom_id       *index;
  int            n_dyes_index;
  int           *n_atoms_dyes_index;

  //Properties for each dye in the trajectory
  //TODO: take out the T as now is clear
  int               n_dyes; //number of dyes in the trajectory
  int              *id_dyes;
  int              *pbcbox_dyes;
  int              *n_atoms_dyes;
  int             **id_atoms_dyes;
  int             **pos_atoms_dyes;
  rvec             *xyz_at0_dyes;
  struct fcspbcbox *pbcbox;

  //(INTERNAL) Speed up and memory control variables
  int    n_dyes_max;   //Maximum number of dyes ever simultaneously present in
                       //this trajectory. For memory allocation
};

struct fcspbcbox { //TODO: probably useless
  //Time dependent variables
  double  intensity_ave; //Average intensity trajectory
  int    *n_dyes_vs_t; //Total number of dyes in a pbcbox as a function of time.
  double *intensity_vs_t; //Total intensity per pbcbox and with time
  double *acf_intensity_time; //Intensity autocorrelation (time average) per box.
                                 // acf= <I(t)-I(0)>t -using fftw
};


/* ###########################
 *    Functions declarations
 * ########################### */
//methods fcs_struct
fcs_struct *init_fcs_struct(int n_fcs_grps, int axis_fcs, int n_pbc_displ,
                                    int pbcboxes, int type_fcs, real focus);
void fcstrj_init(fcs_struct *fcs, int n_trj, int isize_index, atom_id *index,
                  int n_dyes_index, int *n_atoms_dyes_index);
int  populate_fcs_struct (fcs_struct *fcs, gmx_bool bauto_selection,
                              int n_dyes_fcs_grps);
int add_dye_fcs_pbcbox_by_id (fcs_struct  *fcs, int n_trj, int n_pbcbox, int id_dye);
int mv_dyes_due_to_jumps_between_pbcboxes(fcs_struct *fcs, int n_jumps, int (*jumps)[6]);
int gen_rand_sel (int n_dyes, int **shuffle_dye_list);

//Process input files and options
int resolve_axis_fcs( char *normal_axis );
int calulate_pbcboxes (int axis_fcs, int n_pbc_displ);
int find_dest_pbcbox(int axis, int n_pbc_displ, int ori_pbcbox, ivec jump);
int molecular_analysis_index(t_topology top, int isize_index, atom_id *index,
                             int **n_atoms_dyes_index);

//debug & test
void debug_selection (int n_gprs_selected_from_index_file, int *n_dyes_index, int **n_atoms_dyes_index);
void debug_fcs (fcs_struct *fcs);
void print_xyz_at0_new(fcs_struct *fcs, t_trxframe fr);


/* ##################################
 *   Process input files and options
 * ################################## */

/*! @brief Find axis of analysis
 *
 *  @param[in] normal_axis In the fist character contains either x, y, z or o(3d).
 *  @return axis_fcs It can be 0, 1, 2 or 9 (for 3d).
 */
int resolve_axis_fcs( char *normal_axis ) {
  int axis_fcs;
  /* Calculate axis */
  if      (strcmp(normal_axis, "x") == 0){axis_fcs = 0;}
  else if (strcmp(normal_axis, "y") == 0){axis_fcs = 1;}
  else if (strcmp(normal_axis, "z") == 0){axis_fcs = 2;}
  else if (strcmp(normal_axis, "o") == 0){axis_fcs = 9;}
  else {gmx_fatal(FARGS, "Invalid axis, use x, y , z or o");}

  switch (axis_fcs){
    case 0:
      fprintf(stderr, "\nPerforming a 2d analysis: YZ-plane\n\n");
      break;
    case 1:
      fprintf(stderr, "\nPerforming a 2d analysis: XZ-plane\n\n");
      break;
    case 2:
      fprintf(stderr, "\nPerforming a 2d analysis: XY-plane\n\n");
      break;
    case 9:
      fprintf(stderr, "\nPerforming a 3d analysis\n\n");
      break;
    }
    fprintf (stderr, "\n\n######################## AXIS=%d(%s) ###########################\n\n",
             axis_fcs, normal_axis);
    return axis_fcs;
}

/*! @brief Determine the number of pbc boxes in the system which depends
 *  if the system is 3d or 2d and the desired displacement.
 *
 *  @param[in] axis_fcs FCS dimension: 0=yz-plane, 1=xz-plane, 2=xy-planeNumber and 9=3d.
 *  @param[in] n_pbc_displ Displacement along pbc to consider as simulation field
 *                                0=(0,0);
 *                                1={(-1,-1), (0,-1), (1,-1),
 *                                   (-1, 0), (0, 0), (1, 0),
 *                                   (-1, 1), (0, 1), (1, 1)} in 2D
 *  @return pbcboxes Number of fictitious pbcboxes considered in each group (trajectory).\n
 *                   (1+2*n_pbc_displ)^(DIM)
 */
int calulate_pbcboxes (int axis_fcs, int n_pbc_displ) {
  int pbcboxes;

  if (axis_fcs==0 || axis_fcs==1 || axis_fcs==2)
                   pbcboxes = (int) pow (1+2*n_pbc_displ, 2);
  if (axis_fcs==9)  pbcboxes = (int) pow (1+2*n_pbc_displ, 3);

  fprintf (stderr, "########### PBC BOXES IN EACH FCS TRAJECTORY=%d ###############\n\n", pbcboxes);
  return pbcboxes;
}

/*! @brief Function to calculate the destination periodic box for a given jump.
 *
 *  @param[in] axis
 *  @param[in] n_pbc_displ
 *  @param[in] ori_pbcbox Original pbc box before the jump
 *  @jump[in] Vector containing the information about the jump.
 *   Each coordinate refers to what happens in X, Y and Z respectivelly.
 *   Each component can be either: 0=no jump, 1=right jump, -1=left jump.
 *  @return dest_pbcbox The destination pbc box after the jump.
 */
int find_dest_pbcbox(int axis, int n_pbc_displ, int ori_pbcbox, ivec jump) {
  int iD;
  int pow_index=-1;
  int dest_pbcbox;
  int old_row;
  int new_row;

  dest_pbcbox = ori_pbcbox;
  for (iD=0; iD<3; iD++) {
    if (iD == axis) continue;
    pow_index++;

    old_row = dest_pbcbox/ pow(2*n_pbc_displ+1, pow_index+1);

    dest_pbcbox += jump[iD]*pow(2*n_pbc_displ+1,pow_index);
    new_row = dest_pbcbox/ pow(2*n_pbc_displ+1, pow_index+1);

    //Check for negative box number
    if (dest_pbcbox < 0) dest_pbcbox += pow(2*n_pbc_displ+1, pow_index+1);

    //Check for negative jumps
    if (new_row < old_row) dest_pbcbox += pow(2*n_pbc_displ+1, pow_index+1);

    //Check for positive jumps
    if (new_row > old_row) dest_pbcbox -= pow(2*n_pbc_displ+1, pow_index+1);
  }
  if (debug_level >= 3) {
    fprintf (stderr, "\naxis=%d, dislp=%d, ori_pbcbox=%d, jump=(%d,%d,%d)=> dest_pbcbox=%d (FINAL)\n",
                  axis, n_pbc_displ, ori_pbcbox, jump[0], jump[1], jump[2], dest_pbcbox);
  }
  return dest_pbcbox;
}

/*! @brief Determine number of dyes and atoms in each dye in a given index file.
 *
 *  @param[in] top Topology information. We need residue name.
 *  @param[in] isize_index Number of atoms in the index
 *  @param[in] *index Mapping array
 *  @param[out] **n_atoms_dyes_index Pointer to an array of atom number in each dye
 *  @return n_dye Number of dye in the index file (Number of different residues)
 */
int molecular_analysis_index(t_topology top, int isize_index, atom_id *index,
                             int **n_atoms_dyes_index) {
  int i;
  int tmp_resid;
  int n_dye = 0;

  tmp_resid = -1;
  snew(*n_atoms_dyes_index, 1);
  for (i=0; i<isize_index; i++){
    if (top.atoms.atom[index[i]].resind != tmp_resid) { //Find end dye
      if (n_dye%10 ==0)  srenew (*n_atoms_dyes_index, n_dye+10);
      n_dye++;
      (*n_atoms_dyes_index)[n_dye-1]=0;
      tmp_resid = top.atoms.atom[index[i]].resind;
    }
    (*n_atoms_dyes_index)[n_dye-1] ++;
  }

  return n_dye;
}


/* ################################
 *        methods fcs_struct
 * ################################ */

/*! @brief Initialize an array of fcs_struct structures
 *
 *  @param[in] n_fcs_grps Number of "independent" trajectories to analyze.
 *  @param[in] n_pbc_displ Displacement along pbc to consider as simulation field
 *                               (0=(0,0);
 *                                1={(-1,-1), (0,-1), (1,-1),
 *                                   (-1, 0), (0, 0), (1, 0),
 *                                   (-1, 1), (0, 1), (1, 1)} in 2D.
 *  @param[in] axis_fcs FCS dimension: 0=yz-plane, 1=xz-plane, 2=xy-planeNumber and 9=3d.
 *  @param[in] bauto_selection Generate selection automatically.
 *  @param[in] isize_index Number atoms in each index
 *  @param[in] index Mapping to atom number in trajectory
 *  @param[in] n_dyes_index Number of dyes in index file readed
 *  @param[in] n_atoms_dyes_index Number of atoms in every molecule in each index file readed
 *  @return fcs pointer to the array of initialized fcs_struct structures, one per n_fcs_grps.
 */
fcs_struct *init_fcs_struct(int n_fcs_grps, int axis_fcs, int n_pbc_displ, int pbcboxes,
                                    int type_fcs, real focus)
{
  fcs_struct *fcs;

  snew(fcs,1);
  fcs->axis_fcs = axis_fcs;
  fcs->n_pbc_displ = n_pbc_displ;
  fcs->pbcboxes = pbcboxes; //number of pbcboxes per trajectory
  fcs->n_trjs = n_fcs_grps;
  snew(fcs->trj, fcs->n_trjs);
  //FCS parameters
  fcs->type_fcs = type_fcs;
  fcs->focus = focus;

  return fcs;
}

void fcstrj_init(fcs_struct *fcs, int n_trj,
             int isize_index, atom_id *index,
             int n_dyes_index, int *n_atoms_dyes_index)
{
  int iNB; //Counter (N)umber (B)oxes

  //Source index file groups' information
  fcs->trj[n_trj].isize_index = isize_index;
  fcs->trj[n_trj].index = index;
  fcs->trj[n_trj].n_dyes_index = n_dyes_index;
  fcs->trj[n_trj].n_atoms_dyes_index = n_atoms_dyes_index;

  //intialize varaiables for dynamic memory allocation
  fcs->trj[n_trj].n_dyes_max=1;

  //initialize some small amount of memory for several fcs_struct struct variables
  fcs->trj[n_trj].n_dyes=0;
  snew(fcs->trj[n_trj].id_dyes, fcs->trj[n_trj].n_dyes_max);
  snew(fcs->trj[n_trj].pbcbox_dyes, fcs->trj[n_trj].n_dyes_max);
  snew(fcs->trj[n_trj].n_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
  snew(fcs->trj[n_trj].id_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
  snew(fcs->trj[n_trj].pos_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
  snew(fcs->trj[n_trj].xyz_at0_dyes, fcs->trj[n_trj].n_dyes_max);

  //initialize pbcboxes
  snew(fcs->trj[n_trj].pbcbox, fcs->pbcboxes);
  for (iNB=0; iNB < fcs->pbcboxes; iNB++){
    fcs->trj[n_trj].pbcbox[iNB].intensity_ave=0;
    fcs->trj[n_trj].pbcbox[iNB].n_dyes_vs_t=NULL;
    fcs->trj[n_trj].pbcbox[iNB].intensity_vs_t=NULL;
    fcs->trj[n_trj].pbcbox[iNB].acf_intensity_time=NULL;
  }
}

/*! @brief Add dye to a given fcs_struct structure by id
 *
 *  @param[in] fcs Analysis structure
 *  @param[in] n_pbcbox Box where to introduce the dye
 *  @param[in] id_dye Position of dye to add in the index file
 */
int add_dye_fcs_pbcbox_by_id (fcs_struct *fcs, int n_trj, int n_pbcbox, int id_dye)
{
  int iND; //Counter (D)ye (N)umber
  int iND2; //Counter (D)ye (N)umber
  int iDI; //Counter (D)yes (I)ndex
  int iAD; //Counter (A)toms (D)ye
  int id=0; //To find id

  //Check that the dye we want to add is not already in the box
  for (iND=0; iND < fcs->trj[n_trj].n_dyes; iND++) {
    if (fcs->trj[n_trj].id_dyes[iND] == id_dye) {
       //Print details of the error if dupicated dye wants to be inserted
       fprintf (stderr, "List of dyes already in the trajectory:\n");
       for (iND2=0; iND2 < fcs->trj[n_trj].n_dyes; iND2++) {
         fprintf (stderr, "%6d ", fcs->trj[n_trj].id_dyes[iND2]);
         if ((iND2+1)%12 == 0 ) fprintf(stderr,"\n");
       }
       fprintf (stderr, "\n Die that we tried to input again: %d\n", id_dye);

       gmx_fatal(FARGS, "The dye to be added is already in the pbc box!!!",
                       ShortProgram());
       return 0;
    }
  }
  //Add dye to fcs
  fcs->trj[n_trj].n_dyes ++; //Add 1 dye in the fcs

  //reallocate memory if needed
  if (fcs->trj[n_trj].n_dyes > fcs->trj[n_trj].n_dyes_max) {
    fcs->trj[n_trj].n_dyes_max = fcs->trj[n_trj].n_dyes;//Change fcs->n_dyes_max if exceeded
    srenew(fcs->trj[n_trj].id_dyes, fcs->trj[n_trj].n_dyes_max);
    srenew(fcs->trj[n_trj].pbcbox_dyes, fcs->trj[n_trj].n_dyes_max);
    srenew(fcs->trj[n_trj].n_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
    srenew(fcs->trj[n_trj].id_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
    srenew(fcs->trj[n_trj].pos_atoms_dyes, fcs->trj[n_trj].n_dyes_max);
    srenew(fcs->trj[n_trj].xyz_at0_dyes, fcs->trj[n_trj].n_dyes_max);
  }

  //Assign index for new dye
  fcs->trj[n_trj].id_dyes[fcs->trj[n_trj].n_dyes-1] = id_dye;

  //Assign a box to the dye
  fcs->trj[n_trj].pbcbox_dyes[fcs->trj[n_trj].n_dyes-1] = n_pbcbox;

  //Assign number atoms in the new dye
  fcs->trj[n_trj].n_atoms_dyes[fcs->trj[n_trj].n_dyes-1] = fcs->trj[n_trj].n_atoms_dyes_index[id_dye];

  //Allocate and assign index atoms for new dye
  snew(fcs->trj[n_trj].id_atoms_dyes[fcs->trj[n_trj].n_dyes-1], fcs->trj[n_trj].n_atoms_dyes_index[id_dye]);
  //Iteration over all atoms before desired dye = initial position of dye
  for (iDI=0; iDI < id_dye; iDI++) id += fcs->trj[n_trj].n_atoms_dyes_index[iDI];
  //extract the atoms id from our molecule.
  for (iAD=0; iAD < fcs->trj[n_trj].n_atoms_dyes_index[id_dye]; iAD++) {
    fcs->trj[n_trj].id_atoms_dyes[fcs->trj[n_trj].n_dyes-1][iAD] = id + iAD;
  }

  //allocate and assign postion for atoms in dye as in the trajectory
  snew(fcs->trj[n_trj].pos_atoms_dyes[fcs->trj[n_trj].n_dyes-1], fcs->trj[n_trj].n_atoms_dyes_index[id_dye]);
  for (iAD=0; iAD<fcs->trj[n_trj].n_atoms_dyes_index[id_dye]; iAD++) {
    fcs->trj[n_trj].pos_atoms_dyes[fcs->trj[n_trj].n_dyes-1][iAD] =
                     fcs->trj[n_trj].index[fcs->trj[n_trj].id_atoms_dyes[fcs->trj[n_trj].n_dyes-1][iAD]];
  }
  return 1;
}


/*! @brief Move one dye from one pbc box to another
 */

int mv_dyes_due_to_jumps_between_pbcboxes(fcs_struct *fcs,
                                          int n_jumps, int (*jumps)[6])
{
  int iJU; //Counter (JU)mp
  int iD;  //Counter dimensions (3 in general)

  int trj; //Trajectory to be consired
  int ori_pbcbox, dest_pbcbox; //Origin and destination box
  int n_dye; //Number dye in the trajectory
  ivec jump; //tmp information about a jump


  for (iJU=0; iJU< n_jumps; iJU++) {
    trj     = jumps[iJU][0];
    ori_pbcbox = jumps[iJU][1];
    n_dye   = jumps[iJU][2];
    for (iD=0; iD<3; iD++) jump[iD] = jumps[iJU][3+iD];

    //Find destination box for jump
    dest_pbcbox = find_dest_pbcbox(fcs->axis_fcs,fcs->n_pbc_displ, ori_pbcbox, jump);
    fcs->trj[trj].pbcbox_dyes[n_dye] = dest_pbcbox;
  }
  return 0;
}

/*! @brief Generate Random selection of dyes.
 *
 *  Generate a random list of numbers between 0 and [n_dyes-1] without repetition.
 *  Used to generate a random selectiptrDataon of n_dyes for fcs group (Trajectory).
 *  We use Fisher-Yates shuffle to generate n_fcs_grps of the selected dyes without repetition
 *  http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
 *
 *  @param[in] n_dyes Upper limit for the randomized list of natutal number [0, 1, ..., ndyes-1]
 *  @param[out] shuffle_dye_list List of shuffled natural numbers between 0 and n_dyes-1
 */
int gen_rand_sel (int n_dyes, int **shuffled_dye_list){
  int i, tmp_dye;
  real random = 0.0;

  snew(*shuffled_dye_list, n_dyes);
  for (i=0; i<n_dyes; i++) (*shuffled_dye_list)[i]=i; //initialization list

  for (i=n_dyes-1; i>0; i--) {
    random = rand()/(float)RAND_MAX;
    tmp_dye = (*shuffled_dye_list)[i];
    (*shuffled_dye_list)[i] = (*shuffled_dye_list) [(int) (random*i)];
    (*shuffled_dye_list)[(int) (random*i)] = tmp_dye;
  }

  if (debug_level >= 3) {
    fprintf(stderr,"\n############ BEGINNING DEBUG AUTOMATIC SELECTION ###########\n");
    for (i=0; i<n_dyes; i++) {
        fprintf(stderr, "%d ", (*shuffled_dye_list)[i]);
        if ((i+1)%15 == 0 ) fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n############ END DEBUG AUTOMATIC SELECTION ###########\n");
  }

  return 0;
}

/*! @brief Populate an array of fcs_struct structures
 *
 *  @param[in] fcs
 *  @param[in] n_fcs_grps Number of independent trajectories to analyze.
 *  @param[in] bauto_selection
 *  @param[in] n_dyes_fcs_grps
 */
int  populate_fcs_struct (fcs_struct *fcs, gmx_bool bauto_selection,
                              int n_dyes_fcs_grps)
{
  int iX;
  int iTR; //Counter (TR)ajectory
  int iDI; //Counter (D)yes (I)ndex
  int tmp_id_dye;
  int tmp_pbcbox;
  int *tmp_id_atoms_dye;
  int *tmp_pos_atoms_dye;
  int *shuffled_dyes_list;

  /* Generate random list of dyes for automatic selection.
   * List of numbers from 0 to fcs[x].n_dyes_index. */
  if (bauto_selection) {
    //All trajectories obtained from same index group */
    gen_rand_sel (fcs->trj[0].n_dyes_index, &shuffled_dyes_list);
    if (debug_level >= 2) {
      fprintf(stderr,"\n############ BEGINNING DEBUG AUTOMATIC SELECTION ###########\n");
      for (iDI=0; iDI<fcs->trj[0].n_dyes_index; iDI++) {
        fprintf(stderr, "%d ", shuffled_dyes_list[iDI]);
        if ((iDI+1)%15 == 0 ) fprintf(stderr,"\n");
      }
      fprintf(stderr,"\n############ END DEBUG AUTOMATIC SELECTION ###########\n");
   }
  }
  for (iTR=0; iTR<fcs->n_trjs; iTR++){
    if (bauto_selection) //Automatic groups
    {
      tmp_pbcbox = 0;
      for (iX=0; iX< n_dyes_fcs_grps; iX++) {
        tmp_id_dye = shuffled_dyes_list[iX + (iTR*n_dyes_fcs_grps)];
        if (debug_level >= 3) {
          fprintf(stderr,"tmp_shuffle_dyes_list[%d + (%d*%d)]=%d\n",
                  iX, iTR, n_dyes_fcs_grps, tmp_id_dye);
        }
        add_dye_fcs_pbcbox_by_id (fcs, iTR, tmp_pbcbox, tmp_id_dye);
        tmp_pbcbox++;
        if (tmp_pbcbox == fcs->pbcboxes) tmp_pbcbox = 0;
      }
    }
    else //Manual dyes selection from groups in index files
    {
      tmp_pbcbox = 0;
      for (iDI=0; iDI<fcs->trj[iTR].n_dyes_index; iDI++) { //Iteration over the dyes in each input index file.
        tmp_id_dye = iDI;
        add_dye_fcs_pbcbox_by_id (fcs, iTR, tmp_pbcbox, tmp_id_dye);
        tmp_pbcbox++;
        if (tmp_pbcbox == fcs->pbcboxes) tmp_pbcbox = 0;
      }
    }
  }
  return 0;
}


/* ############### PROCESS TRAJECTORY ################*/
/* Remove the jumps over pbc between to given frames.
   gnx = the number of atoms/molecules
   index = the indices of the selection atoms used to remove COMM
   xcur = the current coordinates
   xprev = the previous coordinates
   box = the box matrix */
void rm_jumps_between_2frames(int gnx_com, atom_id index_com[],
                             rvec xcur[], rvec xprev[], matrix box)
{
  int  iX, iD, ind;
  rvec hbox;

  /* Remove periodicity */
  for (iD = 0; (iD < DIM); iD++) {
    hbox[iD] = 0.5*box[iD][iD];
    if (hbox[iD] == 0) {
      gmx_fatal(FARGS, "No box information provided or has a weird 0 size value.",
                ShortProgram());
    }
  }

  for (iX = 0; (iX < gnx_com); iX++) {
    ind = index_com[iX];

    for (iD = DIM-1; iD >= 0; iD--) {
      while (xcur[ind][iD]-xprev[ind][iD] <= -hbox[iD])
      {
        rvec_inc(xcur[ind], box[iD]);
      }
      while (xcur[ind][iD]-xprev[ind][iD] >  hbox[iD])
      {
        rvec_dec(xcur[ind], box[iD]);
      }
    }
  }
}

/*! @brief calculate the center of mass for a group using previous frame
    as reference. Required for COM removal to remove drifting.
 *
 *  @param[in] gnx_com the number of atoms in the selected index file for RmCOMM
 *  @param[in] index index file with the COM removal group
 *  @param[in] xcur current coordinates of all atoms
 *  @param[in] xprev previous coordinates of all atoms
 *  @param[in] box box matrix in this frame
 *  @param[in] atoms atom data (for mass)
 *  @param[out] com current position center of mass of the grup selected for COMM
 */
void calc_com(int gnx_com, atom_id index_com[], rvec xcur[], rvec xprev[],
                     matrix box, t_atoms *atoms, rvec com)
{
  int    iX, iD, ind;
  real   mass;
  double tmass;
  dvec   sx;

  clear_dvec(sx);
  tmass = 0;
  mass  = 1;

  rm_jumps_between_2frames(gnx_com, index_com, xcur, xprev, box);
  for (iX = 0; iX < gnx_com; iX++)
  {
    ind = index_com[iX];
    mass = atoms->atom[ind].m;
    for (iD = 0; iD < DIM; iD++){
        sx[iD] += mass*xcur[ind][iD];
    }
    tmass += mass;
  }
  for (iD = 0; iD < DIM; iD++){
        com[iD] = sx[iD]/tmass;
  }
}


/* ############### FCS ############################## */


void calc_pbcboxes_xyz_displ (int axis_fcs, int n_pbc_displ, int pbcboxes,
                                            int type_fcs, ivec **xyz_displ)
{
  int x, y,z;
  int iD;
  int iNB; //counter number of boxes
  int pbcbox;
  ivec coord;
  int iP[3]; //To move along the displacement matrix
  int displ; //shorter version of n_pbc_displ
  int I_index2box[pbcboxes][pbcboxes];
  int I_index2box_matrix[2*n_pbc_displ+1][2*n_pbc_displ+1][2*n_pbc_displ+1];

  snew (*xyz_displ, pbcboxes);
  //snew (I_index2box, pbcboxes);
  //for (iNB=0; iNB < pbcboxes; iNB++) snew (I_index2box[iNB], pbcboxes);

  //Calculation exchange of coordinates required to generalize the code
  // to use different fcs_axis direction and 3D.
  //coord[0] = index for coordinate x in the new coordinates system
  //coord[1] = index for coordinate y in the new coordinates system
  //coord[2] = index for coordinate z in the new coordinates system
  //e.g. coord[0] = 2 means that coordinate x is now in the third index (y, z, x)
  //     coord[1] = 0 means that coordinate y is now in the first index (y, z, x)
  //     coord[2] = 1 means that coordinate z is now in the second index (y, z, x)
  if (axis_fcs == 0) {coord[0] = 2; coord[1] = 0; coord[2] = 1;}; //-d x => y z |(x)
  if (axis_fcs == 1) {coord[0] = 0; coord[1] = 2; coord[2] = 1;}; //-d y => x z |(y)
  if (axis_fcs == 2) {coord[0] = 0; coord[1] = 1; coord[2] = 2;}; //-d z => x y |(z)
  if (axis_fcs == 9) {coord[0] = 0; coord[1] = 1; coord[2] = 2;}; //-d o => x y z (3d)

  fprintf(stderr, "pbcboxes=%d\n", pbcboxes);

  //Calculation of the displacement matrix to calculate distances between
  // a dye and each of the beams placed in the middle of each pbcbox
  pbcbox=0;
  for (iP[2]=-n_pbc_displ; pbcbox + 1 < pbcboxes; iP[2]++) {
    for (iP[1]=-n_pbc_displ; iP[1] <= n_pbc_displ; iP[1]++) {
      for (iP[0]=-n_pbc_displ; iP[0] <= n_pbc_displ; iP[0]++) {
         pbcbox =  (iP[0]+n_pbc_displ)
                  + (iP[1]+n_pbc_displ) * (2*n_pbc_displ+1)
                  + (iP[2]+n_pbc_displ) * (2*n_pbc_displ+1) * (2*n_pbc_displ+1); //Index of a box
         for (iD=0; (iD<3); iD++) {
           (*xyz_displ)[pbcbox][iD] = iP[coord[iD]]; //Displacement in x y and z adjusted with coord[iD]
         }
         if (axis_fcs != 9)  (*xyz_displ)[pbcbox][axis_fcs] = 0;
      }
    }
  }

  //Debug
  if (debug_level >= 3) {
  int iORI, iREL;
    fprintf(stderr, "\n###### DEBUG interconversion vectors #######\n");
    fprintf(stderr, "\nAxis_fcs=%d => coord[0] = %d; coord[1] = %d; coord[2] = %d\n",
                     axis_fcs, coord[0], coord[1], coord[2]);

    fprintf(stderr, "\nRelative distance matrix (in box size units):\n");
    for (iNB=0; iNB < pbcboxes; iNB++) {
      fprintf(stderr, "Relative pbcbox=%03d xyz_displ=(%2d, %2d, %2d)\n",
                    iNB, (*xyz_displ)[iNB][0], (*xyz_displ)[iNB][1], (*xyz_displ)[iNB][2]);
    }
    fprintf(stderr, "\n###### END DEBUG interconversion vectors #######\n");
  }
}


/*! @brief Calculate the transformation matrix to pass from relative box number
 *         respect the box where the dye is placed to the absolute box number
 *         as defined in the trajectory.
 *
 *  @param[in] axis_fcs
 *  @param[in] n_pbc_displ
 *  @return    Transformation matrix from relative to absolute box number.
                         usage: relative2real_pbcbox_M[box_ABS_ORI][box_REL]=box_ABS.
 */

void calc_relative2real_pbcbox_M (int axis_fcs, int n_pbc_displ, int pbcboxes, int ***relative2real_pbcbox_M) {

  //MatrixXD[ORI][REL]=ABS
  long iX; //loop counter
  long pbcbox_ori; //Ori makes reference to the pbcbox where the dye is
  long pbcbox_relative; //Rel makes releference to the "rel"ative position of a box taking ori as center box
  long pbcbox_absolute; //Abs make reference about the absolute position of the relative box as in the trajectory
  long iORI_1D, iREL_1D,iORI_2D, iREL_2D, iORI_3D, iREL_3D; //Counters for the loops
  long n_1D_pbcboxes = (2*(long)n_pbc_displ+1);
  long n_2D_pbcboxes = n_1D_pbcboxes*n_1D_pbcboxes;
  long n_3D_pbcboxes = n_1D_pbcboxes*n_1D_pbcboxes*n_1D_pbcboxes;
  long **matrix_1D;
  long **matrix_2D;
  long **matrix_3D;
  long N; //to move by squares

  //initialization memory temporal arrays
  snew (matrix_1D, n_1D_pbcboxes);
  for (iX=0; iX < n_1D_pbcboxes; iX++) snew(matrix_1D[iX], n_1D_pbcboxes);
  snew (matrix_2D, n_2D_pbcboxes);
  for (iX=0; iX < n_2D_pbcboxes; iX++) snew(matrix_2D[iX], n_2D_pbcboxes);
  snew (matrix_3D, n_3D_pbcboxes);
  for (iX=0; iX < n_3D_pbcboxes; iX++) snew(matrix_3D[iX], n_3D_pbcboxes);

  //1D relative to absolute
  for (iORI_1D=0; iORI_1D < (2*n_pbc_displ+1); iORI_1D++) {
    //pbcbox_absolute=n_pbc_displ-iORI_1D-1;
    pbcbox_absolute=n_pbc_displ+iORI_1D;
    for (iREL_1D=0; iREL_1D < (2*n_pbc_displ+1); iREL_1D++) {
      pbcbox_absolute++;
      if (pbcbox_absolute >= (2*n_pbc_displ+1)) pbcbox_absolute -= (2*n_pbc_displ+1);
      if (pbcbox_absolute < 0) pbcbox_absolute += (2*n_pbc_displ+1);
      matrix_1D [iORI_1D][iREL_1D]= pbcbox_absolute;
    }
  }
  //2D relative to absolute (Movement in squares of (2*n_pbc_displ+1)**2
  for (iORI_2D=0; iORI_2D < (2*n_pbc_displ+1); iORI_2D++) {
    //N=n_pbc_displ-iORI_2D-1;
    N=n_pbc_displ+iORI_2D;
    for (iREL_2D=0; iREL_2D < (2*n_pbc_displ+1); iREL_2D ++) {
       N++;
      if (N >= (2*n_pbc_displ+1)) N -= (2*n_pbc_displ+1);
      if (N < 0) N += (2*n_pbc_displ+1);
       for (iORI_1D=0; iORI_1D<n_1D_pbcboxes; iORI_1D++){
         for (iREL_1D=0; iREL_1D<n_1D_pbcboxes; iREL_1D++){
           pbcbox_absolute = matrix_1D[iORI_1D][iREL_1D] + n_1D_pbcboxes * N;
           matrix_2D[iORI_2D*n_1D_pbcboxes + iORI_1D][iREL_2D*n_1D_pbcboxes + iREL_1D] = pbcbox_absolute;
         }
       }
    }
  }

  //3D relative to absolute (Movement in squares of (2*n_pbc_displ+1)**3
  for (iORI_3D=0; iORI_3D < (2*n_pbc_displ+1); iORI_3D++) {
    //N=n_pbc_displ-iORI_3D-1;
    N=n_pbc_displ+iORI_3D;
    for (iREL_3D=0; iREL_3D < (2*n_pbc_displ+1); iREL_3D ++) {
       N++;
       if (N >= (2*n_pbc_displ+1)) N -= (2*n_pbc_displ+1);
       if (N < 0) N += (2*n_pbc_displ+1);
       for (iORI_2D=0; (iORI_2D < n_2D_pbcboxes); iORI_2D++){
         for (iREL_2D=0; (iREL_2D < n_2D_pbcboxes); iREL_2D++){
           pbcbox_absolute = matrix_2D[iORI_2D][iREL_2D] + n_2D_pbcboxes * N;
           //fprintf(stderr, "[1]=%d [2]=%d\n", iORI_3D*n_2D_pbcboxes+iORI_2D, iREL_3D*n_2D_pbcboxes+iREL_2D);
           matrix_3D[iORI_3D*n_2D_pbcboxes+iORI_2D][iREL_3D*n_2D_pbcboxes+iREL_2D] = pbcbox_absolute;
         }
       }
    }
  }

  //Pass pointer to output matrix depending of 3D or 2D case.
  if (axis_fcs == 9) {
    snew (*relative2real_pbcbox_M, n_3D_pbcboxes);
    for (pbcbox_ori=0; pbcbox_ori < n_3D_pbcboxes;pbcbox_ori++){
      snew ((*relative2real_pbcbox_M)[pbcbox_ori], n_3D_pbcboxes);
      for (pbcbox_relative=0; pbcbox_relative < n_3D_pbcboxes;pbcbox_relative++){
        (*relative2real_pbcbox_M)[pbcbox_ori][pbcbox_relative]= matrix_3D[pbcbox_ori][pbcbox_relative];
      }
    }
  }  else {
    snew (*relative2real_pbcbox_M, n_2D_pbcboxes);
    for (pbcbox_ori=0; pbcbox_ori < n_2D_pbcboxes;pbcbox_ori++){
      snew ((*relative2real_pbcbox_M)[pbcbox_ori], n_2D_pbcboxes);
      for (pbcbox_relative=0; pbcbox_relative < n_2D_pbcboxes;pbcbox_relative++){
        (*relative2real_pbcbox_M)[pbcbox_ori][pbcbox_relative]= matrix_2D[pbcbox_ori][pbcbox_relative];
      }
    }
  }
  //Debug
  if (debug_level >= 2) {
    int iORI, iREL;
    fprintf(stderr, "\n###### DEBUG interconversion matrixes relative to real#######\n");
    fprintf(stderr, "\nMatrix Relative box (x) to real pbcbox number using origin box(y):\n   ");
    for (iREL=0; iREL < pbcboxes; iREL++) {
        fprintf(stderr, "%2d ", iREL);
    }
    fprintf(stderr, "\n");
    for (iORI=0; iORI < pbcboxes; iORI++) {
      fprintf(stderr, "%2d ", iORI);
      for (iREL=0; iREL < pbcboxes; iREL++) {
        fprintf(stderr, "%2d ", (*relative2real_pbcbox_M) [iORI][iREL]);
      }
    fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n###### END DEBUG interconversion matrixes relative to real#######\n");
  }
  if (debug_level >= 3) {
    fprintf(stderr, "\n######### DEBUG Box numbering squeme interconversion matrix ######:\n"\
                    "relative2real_pbcbox_M[pbcbox_dyeX_number][relative_number_pbcbox_respect_dyeX]=pbcbox_number\n"\
                    "Provides the real number of a pbcbox when referred to respect a dye and we\n"\
                    "it is assubem that the dye is always in the central box:\n\n"\
                    "[y-axis] Real pbcbox dyeX: box number where dye is located inside its trajectory\n"\
                    "         The numbering starts at 0 and increases first x then y and finally z.\n"\
                    "         It depends only in the original positioning and trajectory\n"\
                    "[x-axis] Relative pbcbox respect dyeX: Box number neigbours boxes (dye box taken as central)\n"\
                    "         The number of the box containing the dyes is the central one=>\n"\
                    "         1D:(n_pbc_displ+1)*[1]\n"\
                    "         2D:(n_pbc_displ+1)*[1 +(2*n_pbc_displ+1)]\n"\
                    "         3D:(n_pbc_displ+1)*[1 +(2*n_pbc_displ+1)+(2*n_pbc_displ+1)**2]\n"\
                    "**The matrix is symmetric respect diagonal\n");
    //Print 1D matrix
    fprintf(stderr, "\nMatrix 1D:\n   ");
    for (pbcbox_relative=0; pbcbox_relative < n_1D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", pbcbox_relative);
    }
    fprintf(stderr, "\n");
    for (pbcbox_ori=0; pbcbox_ori < n_1D_pbcboxes; pbcbox_ori++) {
      fprintf(stderr, "%2d ", pbcbox_ori);
      for (pbcbox_relative=0; pbcbox_relative < n_1D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", matrix_1D [pbcbox_ori][pbcbox_relative]);
      }
      fprintf(stderr, "\n");
    }
    //Print 2D matrix
    fprintf(stderr, "\nMatrix 2D:\n   ");
    for (pbcbox_relative=0; pbcbox_relative < n_2D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", pbcbox_relative);
    }
    fprintf(stderr, "\n");
    for (pbcbox_ori=0; pbcbox_ori < n_2D_pbcboxes; pbcbox_ori++) {
      fprintf(stderr, "%2d ", pbcbox_ori);
      for (pbcbox_relative=0; pbcbox_relative < n_2D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", matrix_2D [pbcbox_ori][pbcbox_relative]);
      }
      fprintf(stderr, "\n");
    }
    //Print 3D matrix
    fprintf(stderr, "\nMatrix 3D:\n   ");
    for (pbcbox_relative=0; pbcbox_relative < n_3D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", pbcbox_relative);
    }
    fprintf(stderr, "\n");
    for (pbcbox_ori=0; pbcbox_ori < n_3D_pbcboxes; pbcbox_ori++) {
      fprintf(stderr, "%2d ", pbcbox_ori);
      for (pbcbox_relative=0; pbcbox_relative < n_3D_pbcboxes; pbcbox_relative++) {
        fprintf(stderr, "%2d ", matrix_3D [pbcbox_ori][pbcbox_relative]);
      }
      fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n######### END DEBUG Box numbering squeme interconversion matrix ######:\n\n");
  }
}


/*! @brief Calculates the intensity of a given dye
 *
 *  The intensity depends not only in the distance from the beam but also in the shape
 *   of the beam and the kind of emision done.
 *
 *  @param[in] type Type of beam: 0: gaussian, 1: flat, 2: qm-style
 *  @param[in] focus Radius of the focus beam in nm.
 *  @param[in] distance Distance between a beam and a dye
 *  @return intensity instantaneous intensity reported by the dye.
 */

float calc_intensity_dye(int type_fcs, real focus, rvec dist_beam, real inv_focus2_extra)
{
  real random;

  if ( type_fcs == 0 ) { //Gaussian beam
    return exp(norm2(dist_beam)*inv_focus2_extra);
  }
  else if ( type_fcs == 1 ) { //Stept Beam
    if( norm(dist_beam) < focus ) {
      return 1;
    }
    else {
      return 0;
    }
  }
  else if( type_fcs == 2 ) { //Gaussian beam(quantum aproach)
    random = rand()/(float)RAND_MAX;
    if( exp(norm2(dist_beam)*inv_focus2_extra) > random ) {
      return 1;
    }
    else {
      return 0;
    }
  }
  else {
          gmx_fatal(FARGS, "Beam type not implemented",  ShortProgram());
  }
}

/*! @brief Calculate autocorrelation function using fftw
 *
 */
double *calc_acf_fftw(long steps, double *intensity, double intensity_ave,
            fftw_complex *io, fftw_plan plan_FT, fftw_plan plan_iFT) {
  double *acf;
  int iS;  //Counter time (S)teps
  snew(acf, steps);
  for (iS=0; (iS < steps); iS++){
    io[iS][0] = intensity[iS]-intensity_ave;
    io[iS][1] = 0;
  }
  //Usage of zero paddind of step size to avoid contamination due periodicity
  for (iS=steps; (iS < 2*steps); iS++){
    io[iS][0] = 0;
    io[iS][1] = 0;
  }
  fftw_execute(plan_FT);
  //Calculation of acf=FT x *FT = (a+bi) x (a-bi) = a^2+b^2
  for (iS=0; (iS < 2*steps); iS++){
    io[iS][0] = (io[iS][0]*io[iS][0])+(io[iS][1]*io[iS][1]);
    io[iS][1] = 0;
  }
  fftw_execute(plan_iFT);
  for (iS=0; (iS < steps); iS++){
    //-We normalize the acf by 2*step (=number of elements) as fftw does not make any normalization.
    //  => This provides us  unnormalized_acf (tau) =Sum(0,T-tau) I(tau+0)*I(tau+T)
    //-We normalize then to obtain acf(tau)= unnormalized_acf (tau)/ (T-tau)
    acf[iS] = io[iS][0] / ((double)(2*steps) * (double)(steps-iS));
  }
  return acf;
}

/* ################ PRINT RESULTS in  fcs struct ############## */

void print_results_fcs (fcs_struct fcs[], char prefix[])
{
//TODO: move all printing code here
}
/* ################ DEBUG & TEST ####################### */

/*! @brief Debug selection
 *
 *  @param[in]  n_gprs_selected_from_index_file
 *  @param[in]  n_dyes_index
 *  @param[in]  n_atoms_dyes_index
 */
void debug_selection (int n_gprs_selected_from_index_file, int *n_dyes_index, int **n_atoms_dyes_index) {
  int i,j;
  fprintf(stderr,"\n############ BEGINNING DEBUG INFO SELECTION ###########\n");
    for (i=0; i < n_gprs_selected_from_index_file; i++) {
      fprintf(stderr,"\nNumber of molecules selected %d in group %d\n", n_dyes_index[i], i);
      fprintf(stderr,"Number atoms in molecule:\n");
      for (j=0; j < n_dyes_index[i]; j++) {
        fprintf(stderr,"n_atoms_dye[%d][%d]=%d   ",i ,j, n_atoms_dyes_index[i][j]);
        if ((j+1)%3 == 0 ) fprintf(stderr,"\n");
      }
    }
    fprintf(stderr,"\n############ END DEBUG INFO SELECTION ###########\n");
}

/*! @brief Prints content array of fcs scructs
 *
 *  @param[in] fcs Array of fcs structures
 *  @param[in] n_fcs_grps Number of fcs structs in the array
 */
void debug_fcs (fcs_struct *fcs) { //Debug info for initialization content analysys groups
  int iTR; //Counter (TR)ajectory
  int iND; //Counter (D)ye (N)umber
  int iAD; //Counter (A)toms (D)ye
  fprintf(stderr,"\n############ BEGINNING DEBUG INFO ANALYSIS GROUP ###########\n");
  fprintf(stderr, "fcs->n_trjs=%i\n",fcs->n_trjs);
  for (iTR=0; iTR<fcs->n_trjs; iTR++){
    fprintf(stderr, "fcs->trj[%i]->n_dyes_index=%i\n",iTR, fcs->trj[iTR].n_dyes_index);
  }
  for (iTR=0; iTR < fcs->n_trjs; iTR ++) { //Populate each group analysis
    fprintf (stderr, "Trajectory = %d => n_dyes=%d\n", iTR, fcs->trj[iTR].n_dyes);
    for(iND=0; (iND < fcs->trj[iTR].n_dyes); iND++) {
      fprintf (stderr, "dye= %d box=%d id_dye=%d number_atoms=%d\n", iND,
                       fcs->trj[iTR].pbcbox_dyes[iND],
                       fcs->trj[iTR].id_dyes[iND],
                       fcs->trj[iTR].n_atoms_dyes[iND]);
      fprintf (stderr, "id_atoms_dye= {");
      for(iAD=0; (iAD < fcs->trj[iTR].n_atoms_dyes[iND]); iAD++) {
        fprintf (stderr, "%d ", fcs->trj[iTR].id_atoms_dyes[iND][iAD]);
      }
      fprintf (stderr, "}\n");
      fprintf (stderr, "pos_atoms_dye= {");
      for(iAD=0; (iAD < fcs->trj[iTR].n_atoms_dyes[iND]); iAD++) {
        fprintf (stderr, "%d ", fcs->trj[iTR].pos_atoms_dyes[iND][iAD]);
      }
      fprintf (stderr, "}\n");
    }
    fprintf (stderr, "-------------------------------\n");
  }
  fprintf(stderr,"\n############ END DEBUG INFO ANALYSIS GROUP ###########\n");
}

/*! @brief Prints position of the dyes in a given frame.
 *
 */
void print_xyz_at0_new(fcs_struct *fcs, t_trxframe fr){
  int iTR; //Counter (TR)ajectory
  int iND; //Counter (D)ye (N)umber

  fprintf(stderr, "\n\n #### Initial position dyes##\n");
  for (iTR=0; (iTR < fcs->n_trjs) ; iTR++){
    for (iND=0; iND < fcs->trj[iTR].n_dyes; iND++){
      fprintf(stderr, "n_trj=%d dye=%d box=%d (%3.5f, %3.5f, %3.5f)\n", iTR, iND,
                      fcs->trj[iTR].pbcbox_dyes[iND],
                      fcs->trj[iTR].xyz_at0_dyes[iND][0],
                      fcs->trj[iTR].xyz_at0_dyes[iND][1],
                      fcs->trj[iTR].xyz_at0_dyes[iND][2]);
    }
  }
}



/*! @brief Calculates the FCS curve from MD simulation for selected molecules (dyes).
 */
int main(int argc,char *argv[])
{
  const char *desc[] = {
    "Calculates the FCS curve from MD simulation for selected molecules (dyes)."
  };
  /* #####################
   * DECLARATION VARIABLES
   * ###################### */

  //Counters and others
  int iX;  //Counter general usage
  int iTR; //Counter (TR)ajectory
  int iND; //Counter (N)umber of the (D)ye
  int iNB; //Counter (N)umber (B)oxes
  int iDI; //Counter (D)yes (I)ndex
  int iAD; //Counter (A)toms (D)ye
  int iGS; //Counter number of (G)oups (S)elected from index file
  int iD;  //Counter dimensions (3 in general)
  int iS;  //Counter time (S)teps
  int iTAU; //Counter lag time


  real         random = 0.0; //to store the random numbers

  //gromacs variables
  output_env_t oenv;

  //Read top
  t_topology   top;
  rvec         *xtop;
  char         title[STRLEN];

  //Read index file (ndx)
  int          n_gprs_selected_from_index_file;
  int          *isize_index;  //Number atoms in each index
  atom_id      **index; //Mapping to atom number in trajectory
  char         **grpname_index; //Name of each selected group from index
  int          *n_dyes_index;   //Number of dyes in the groups selected from index file
  int          **n_atoms_dyes_index;   //Number of atoms in every molecule in each index file readed

  //Read gromacs trajectory
  t_trxstatus  *status;
  int          flags = TRX_READ_X;
  t_trxframe   fr;

  //Deal with pbc in gromacs
  int          ePBC;
  gmx_rmpbc_t  gpbc;
  t_pbc        pbc;
  matrix       box;

  //Generate Analisis groups
  gmx_bool     bauto_selection; //
  int          n_grps = 0;      //Number of groups to analyze
                                //=0 for automatic group generator.
                                //!=0 one group per index file provided.
  int          n_dyes_fcs_grps = 0;  //Number of dyes initially assigned to each analysis group
                                     //With "-fakepbc" option this value can change during
                                     //the analysis. n_dyes_fcs_grp is then the average number.
  int          n_fcs_grps;      //Number of groups for FCS analysis.
                                //Equal to n_gprs if grous are selected from index file.
                                //Equal to molecs/ndyes if groups are automatically generated
/*
  fcs_struct      *fcs;         //Array structures containing topology information about dyes
                                //one for each diferent FCS analysis trajectories.
*/
  fcs_struct   *fcs;        //TODO: For new class

  //Remove center of mass motion (rmcomm ) option
  static gmx_bool bRmCOMM = FALSE; //Remove center of mass motion of a selection group.
  int           *gnx_com     = NULL; /* the COM removal group size  */
  atom_id      **index_com   = NULL; /* the COM removal group atom indices */
  char         **grpname_com = NULL; /* the COM removal group name */
  rvec           com_sel_cur;        /* center of mass position current frame */
  rvec           com_sel_prev;       /* center of mass position previous frame */
  rvec           comm_sel;           /* center of mass motion between two contiguous frames */
  rvec           comm_sel_cumulative;/* Accomulated center of mass motion selection rmcomm */
  rvec          *xcur;               /* the coordinates to calculate displacements for */
  rvec          *xprev;              /* the coordinates to calculate displacements for */

  //Process trajectory
  static gmx_bool bTrjCOM = FALSE; //Remove center of mass motion of a selection group.
  long         step;             //Counter number of frames analyzed.
  float       *simulation_time=NULL; //Time in each processed simulation frame
  rvec         com_dye;              //Used to calculate the center of mass of a dye
  rvec         dist_c_beam;      //Distance between the dye and central a beam [beam in the dye's box]
  int          step_max;         /* Variable used to allocate memory during loop over the trajectory.
                                  *  for step dependent arrays. */
  float         box_ave[3];          //To calculate average box for log file

  //Variables to find pbc jumps
  real         movement;          //Tmp variable to record the displacement between two frames
  ivec         jump;              //Tmp variable to record a jump
  gmx_bool     bjump = FALSE;     //Tmp Variable used to indicate when a jump has ocurred
  int          n_jumps;           //Number of jumps in a frame
  int          (*jumps)[6];       //Array containing the informations of all jumps in a frame
  int          n_jumps_max=1000;     //To optimize speed and memory usage


  //Parameters FCS  experiment
  int          axis_fcs;        //FCS dimension: 0=yz-plane, 1=xz-plane, 2=xy-planeNumber and 9=3d
  int          n_pbc_displ = 0; /* Displacement along pbc to consider as simulation field
                                 * 0=(0,0);
                                 * 1={(-1,-1), (0,-1), (1,-1),
                                 *    (-1, 0), (0, 0), (1, 0),
                                 *    (-1, 1), (0, 1), (1, 1)} in 2D. */
  int          pbcboxes = 0;   /* Number of fictitious pbcboxes considered in each group (trajectory).
                                 * (1+2*n_pbc_displ)^(DIM) */
  static char  *normal_axis[] = { NULL,
                                 "z", "y", "x",//direction normal plane (2d).
                                 "o",          //3d
                                 NULL };       //z is default because first
  int          type_fcs = 0;    //Type of beam: 0: gaussian, 1: flat, 2: qm-style
  ivec         *xyz_displ;      //Array containing the displacements corresponding to neighboughring boxes.
  int          **relative2real_pbcbox_M; //Interconversion matrix from relative pbcbox number respect to
                                         // a given dye to abosulte pbcbox number as in trajectory.
              //relative2real_pbcbox_M[pbcbox_dyeX_number][relative_number_pbcbox_respect_dyeX]=pbcbox_number
// static char  *bc[] = { NULL,
//                        "pbc",    //Periodic boundary conditions outer simulation cells
//                        "nojump", //Dyes can freely diffuse, no_jump
//                        "mc",     //montecarlo
//                        "none",   //Dyes are removed when crossing outter simuation cells border
//                        NULL };
  rvec         beam_center;       //Position of the beam center in the box
  real         focus = 5.0;       //Radius of the focus beam in nm.
  int         *n_dyes_G_vs_t;     //Sum of the number dyes in all the trajectories and boxes (Time series)
  double       intensity = 0.0;   //Parameted used to record the instantaneous intensity of a dye
  fftw_complex *io;               //To perform the fft calculations
  fftw_plan    plan_FT, plan_iFT; //Plan to perfom fftw calculation
  double      *acf_intensity_time_B_ave;
  double      *acf_intensity_time_T_ave; //(Debug)
  rvec         dist_beam;          //Used when calculating the intensity of a dye
  int          real_pbcbox;        //Used when calculating the intensity of a dye
  float        inv_focus2_extra;   //Used when calculating the intensity of a dye (Speed up variable)

  //Output files
  static gmx_bool bodyes = FALSE; //Output statistics dyes numbers.
  static gmx_bool boint = FALSE; //Output statistics intensities.
  static gmx_bool bopbcbox = FALSE; //Output results also for each pbcbox (Very large files).
  static const char  *prefix = "trj"; //Prefix name for all output files
  char         name[100]; //variable to contruct filenames
  FILE         *olog = NULL;   //Summary of input and output.
  FILE         *odyes_B = NULL; //Output file number of dyes in each pbc Box vs time
  FILE         *odyes_B_ave = NULL; //Output files for average values n_dyes in each pbcbox
  FILE         *oint_B = NULL;   //Output file intensity recorded in each pbc Box vs time
  FILE         *oint_B_ave = NULL;   //Output file average intensity recorded in each pbc Box vs time
  FILE         *oacf_B = NULL; //Output acf with time pbcboxes
  FILE         *oacf_B_ave = NULL; //Output average acf with time pbcboxes
  FILE         *oacf_ensemble = NULL; //Output average acf with time pbcboxes
  char         xlabel [100]; //Variable to manipulate label plots

  //add option to the program
  t_pargs pa[] = {
    { "-ngroups", FALSE, etINT, {&n_grps},
      "Number of dyes' groups (one per selection). If 0 then automatic selection => [Total number molec]/ndyes"
    },
    { "-ndyes", FALSE, etINT, {&n_dyes_fcs_grps},
      "Number of dyes per analysis group (Only for automatic selection)"
    },
     { "-npbc", FALSE, etINT, {&n_pbc_displ},
      "Number of periodic displacements/images to be used around central simulation box"
    },
    { "-d",   FALSE, etENUM, {normal_axis},
          "Direction of the normal to the analysis plane or 3d(o)"
    },
//   { "-bc",   FALSE, etENUM, {bc},
//         "Treatment boundary conditions:"
//   },
    { "-type", FALSE, etINT, {&type_fcs},
      "Type of beam: 0: gaussian, 1: flat, 2: qm-style"
    },
    { "-focus", FALSE, etREAL, {&focus},
      "Radius of the focus beam"
    },
    { "-prefix", FALSE, etSTR, {&prefix},
      "Prefix name for all output files"
    },
    { "-rmcomm", FALSE, etBOOL, {&bRmCOMM},
          "Remove center of mass motion"
    },
    { "-trjcom", FALSE, etBOOL, {&bTrjCOM},
          "If input trajectory contains already CM molecules"
    },
    { "-odyes", FALSE, etBOOL, {&bodyes},
          "Output statistics dyes numbers"
    },
    { "-oint", FALSE, etBOOL, {&boint},
          "Output statistics intensities"
    },
    { "-opbcbox", FALSE, etBOOL, {&bopbcbox},
          "Output statistics per each pbcbox"
    },
  };

  //Input and Output files program
  t_filenm fnm[] = {
    { efTPS,  NULL,  NULL, ffREAD },
    { efTRX, "-f", NULL, ffREAD },
    { efNDX, "-n", NULL, ffREAD},
  };
  #define NFILE asize(fnm)

  CopyRight(stderr,argv[0]);

  //Parse inputed arguments to gromacs
  parse_common_args(&argc,argv,
                    PCA_CAN_TIME | PCA_CAN_VIEW,
		    NFILE,fnm,asize(pa),pa,asize(desc),desc,0,NULL, &oenv);

  //Open FCS.log - Where to store anything worth saving.
  sprintf(name, "%s.log", prefix);
  olog = fopen(name, "w");
  fprintf(olog, "SUMMARY g4_FCS\n");
  fprintf(olog, "------------------\n");
  fprintf(olog, "\nABBREVIATIONS: \n");
  fprintf(olog, "g = global\n");
  fprintf(olog, "t/ts = trajectory/es\n");
  fprintf(olog, "b/bs = pbcbox/es\n");

  fprintf(olog, "\nINPUT OPTIONS:\n");
  fprintf(olog, "n_groups = %d\n", n_grps);
  fprintf(olog, "ndyes per trajectory= %d\n", n_dyes_fcs_grps);
  fprintf(olog, "npbc = %d\n", n_pbc_displ);
  fprintf(olog, "fcs type = %d\n", type_fcs);
  fprintf(olog, "radius focus beam (nm)= %f\n",focus);
  fprintf(olog, "axis parallel beam= %s\n", normal_axis[0]);


  //initialize random generator
  fprintf(stderr, "Seed in random number generator=%d\n", (unsigned)time( NULL ));
  srand( (unsigned)time( NULL ) ); //random seed
                                   //srand have problems in openmp

  //Read topology file
  read_tps_conf(ftp2fn(efTPS,NFILE,fnm),title,&top,&ePBC,&xtop,NULL,box,TRUE);
  sfree(xtop);

  //Select group for center of mass removal
  if (bRmCOMM)
  {
    snew(gnx_com, 1);
    snew(index_com, 1);
    snew(grpname_com, 1);

    fprintf(stderr, "\nNow select a group for center of mass removal:\n");
    get_index(&top.atoms, ftp2fn(efNDX,NFILE,fnm), 1, gnx_com, index_com, grpname_com);
  }

  /* Read index file/s containing fluorescents dyes to consider */
  if (n_grps < 0) gmx_fatal(FARGS, "Number of groups cannot be negative",  ShortProgram());
  n_gprs_selected_from_index_file =  (n_grps==0?1:n_grps);
  snew(isize_index, n_gprs_selected_from_index_file);
  snew(index, n_gprs_selected_from_index_file);
  snew(grpname_index, n_gprs_selected_from_index_file);
  if (n_grps > 0){
    fprintf(stderr,"\nSelect the %d group(s) of fluorescent dyes to calculate different FCS profiles:\n",
                    n_gprs_selected_from_index_file);
  } else {
    fprintf(stderr,"\nSelect the group from which select the fluorecent dyes:\n\n");
  }
  get_index(&top.atoms, ftp2fn(efNDX,NFILE,fnm), n_gprs_selected_from_index_file,
            isize_index, index, grpname_index);

  /* Analyze flourescent dyes from index files:
   * - Extraction of number of dyes in each index file
   * - Extraction number of atoms that each dye has. */
  snew(n_dyes_index, n_gprs_selected_from_index_file);
  snew(n_atoms_dyes_index, n_gprs_selected_from_index_file);
  for (iGS=0; iGS < n_gprs_selected_from_index_file; iGS++) {
    n_dyes_index [iGS] = molecular_analysis_index(top, isize_index[iGS], index[iGS],
                                                 &(n_atoms_dyes_index[iGS]));
  }
  if (debug_level >= 2 ) debug_selection(n_gprs_selected_from_index_file,
                                                 n_dyes_index, n_atoms_dyes_index);

  //Determine axis and dimension for FCS analysis
  axis_fcs = resolve_axis_fcs (normal_axis[0]);

  /* Determine number pbcboxes to consider inside each fcs trajectory */
  pbcboxes = calulate_pbcboxes(axis_fcs, n_pbc_displ);

  /* Determine wether to use auto or manual selection &
   * Determine the number of independent trajectories to calculate FCS */
  if (n_grps == 0) {
    if (n_dyes_fcs_grps <= 0) {
        gmx_fatal(FARGS, "[-ndyes] is compulsory for automatic selections [-ngroups 0] and must be positive",
                  ShortProgram());
    }
    if ( n_dyes_fcs_grps > n_dyes_index[0]) {
      gmx_fatal(FARGS, "-ndyes value bigger than number of dyes in the index file provided.",
                       ShortProgram());
    }
    bauto_selection = TRUE; //set automatic selection dyes FCS groups
    n_fcs_grps = n_dyes_index[0] / n_dyes_fcs_grps;
  } else {
    if ( n_dyes_fcs_grps != 0) {
      gmx_fatal(FARGS, "[-ndyes] has no effect when using manual selection (-ngrps !=0).\n"
                       "Please set it to zero [Default value]",
                       ShortProgram());
    }
    bauto_selection = FALSE; //Manual selection
    n_fcs_grps = n_grps;
  }
  fprintf(stderr, "Number of trajectories used to calculate FCS=%d\n", n_fcs_grps);

  /* Initialization of the fcs_struct structures, one per each n_fcs_grps.
   * This structure will contain not only all information needed for calculating
   * the FCS but also will be used to store the data of the FCS calculation.
   * After this initialization is basically empty, it only will have information
   * of the FCS geometry and the input index files but to do anything useful it
   *  requires to be and needs to be properly populated. */
  fcs = init_fcs_struct(n_fcs_grps, axis_fcs, n_pbc_displ, pbcboxes,
                                type_fcs, focus);
  for (iTR=0; iTR < fcs->n_trjs; iTR++) {
    if (bauto_selection){
      fcstrj_init(fcs, iTR, isize_index[0], index[0], n_dyes_index[0], n_atoms_dyes_index[0]);
    } else {
      fcstrj_init(fcs, iTR, isize_index[iTR], index[iTR], n_dyes_index[iTR], n_atoms_dyes_index[iTR]);
    }
  }
  if(debug_level >= 2) {fprintf(stderr, "\nINIT:"); debug_fcs (fcs);};
  /* Populate the fcs_struct corresponding to each trajectories. The method of
   * population can be either automatic if -ngroups==0 or manual for -ngroups > 0.
   * For the automatic case the dyes in the index files will be distributed among
   * trajectories, -ndyes per group. For the manual case each trajectory will
   * contain exactly the dyes iside the index file group selected for each trajectory.
   * The dyes of a group(trajectory) will be distributed evently among all simulation
   * boxes. It is responsibility of the user to ensure that the number of dyes is
   * proportional to the number of boxes to avoid different initial densities between
   * pbcboxes. */
  populate_fcs_struct (fcs, bauto_selection, n_dyes_fcs_grps);
  if(debug_level >= 2){fprintf(stderr, "\nPOPULATE_new:"); debug_fcs (fcs);};

  //test code add and remove of dyes
  //if(debug_level >=  3) debug_rm_add_fcs_pbcbox_dye (fcs, n_fcs_grps, n_atoms_dyes_index, index);

  //initialized exchange matrix and coord vector to speed up Intensity calculation
  calc_pbcboxes_xyz_displ(fcs->axis_fcs, fcs->n_pbc_displ, fcs->pbcboxes,
                          fcs->type_fcs, &xyz_displ);
  //Calculate the transformation matrix to pass from relative box number
  //respect the box where the dye is placed to the absolute box number
  //as defined in the trajectory. relative2realM[box_ABS_ORI][box_REL]=box_ABS
  //relative2real_pbcbox_M = calc_relative2real_pbcbox_M (axis_fcs, n_pbc_displ);
  calc_relative2real_pbcbox_M(fcs->axis_fcs, fcs->n_pbc_displ, fcs->pbcboxes, &relative2real_pbcbox_M);

  //Print some data in FCS.log
  fprintf(olog, "Number trajectories = %d (nTr)\n", fcs->n_trjs);
  fprintf(olog, "Number pbcboxes per trajectory = %d(nPBCboxes)\n",  fcs->pbcboxes);
  fprintf(olog, "Total number mesurements = %d(nTr * nPBCboxes)\n",  fcs->n_trjs* fcs->pbcboxes);


  /*****************************************************************************
  *                    READ AND PROCESS THE TRAJECTORY                         *
  *****************************************************************************/
  //Read first frame and initialize rmpbc function (molecules whole)
  read_first_frame(oenv, &status,ftp2fn(efTRX,NFILE,fnm),&fr,flags);
  if (!bTrjCOM) gpbc = gmx_rmpbc_init(&top.idef, ePBC, fr.natoms,fr.box);

  //Some initializations
  step=0;
  step_max = 10; //For dynamic memory allocation
  //the following lines had no effect in the segcore, in principle.
  snew(simulation_time, step_max);
  for (iTR=0; iTR < fcs->n_trjs; iTR++){
    for (iNB=0; iNB < fcs->pbcboxes; iNB++){
      if (bodyes) {snew(fcs->trj[iTR].pbcbox[iNB].n_dyes_vs_t, step_max);}
      snew(fcs->trj[iTR].pbcbox[iNB].intensity_vs_t, step_max);
    }
  }
  bjump=FALSE;
  for (iD=0; iD<3; iD++) jump[iD]=0;
  n_jumps_max=0;
  snew(jumps,n_jumps_max);
  if (bRmCOMM)
  {
    clear_rvec(com_sel_cur);
    clear_rvec(com_sel_prev);
    clear_rvec(comm_sel);
    clear_rvec(comm_sel_cumulative);
    snew(xcur, fr.natoms);
    snew(xprev, fr.natoms);
    memcpy(xprev, fr.x, fr.natoms*sizeof(xprev[0]));
  }
  //initialize variables which depend on the steps

  //initialize pbc structute: Used by gmx_rmpbc and pbc_dx
  set_pbc(&pbc,ePBC,fr.box); //

  //make molecules whole. Assumed before any calculation.
  if (!bTrjCOM) gmx_rmpbc(gpbc, fr.natoms,fr.box,fr.x); //Make molecule whole


  /* Populate the initial positions of each dyes at step=0.
   * This could be done in the loop but is cheaper and cleaner here. */
  for (iTR=0; (iTR < fcs->n_trjs) ; iTR++){
    for (iND=0; iND < fcs->trj[iTR].n_dyes; iND++){
      fcs->trj[iTR].xyz_at0_dyes[iND][0] = fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][0];
      fcs->trj[iTR].xyz_at0_dyes[iND][1] = fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][1];
      fcs->trj[iTR].xyz_at0_dyes[iND][2] = fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][2];
    }
  }
  if (debug_level >= 3) print_xyz_at0_new(fcs, fr);

  //Few calculation to speed up the main loop
  inv_focus2_extra=-2/(fcs->focus*fcs->focus); //Used to speed up the calculation of the intensity
  if (fcs->type_fcs != 0) gmx_fatal(FARGS, "Beam type desacivated in the code for performance reasons",  ShortProgram());

  /*#################################
   *# Main loop over the trajectory #
   *#################################*/
  do {

    //Allocate memory if needed
    if (step >= step_max) {
      step_max += 10000;
      srenew(simulation_time, step_max);
      for (iTR=0; iTR < fcs->n_trjs; iTR++){
        for (iNB=0; iNB < fcs->pbcboxes; iNB++){
          if (bodyes) {srenew(fcs->trj[iTR].pbcbox[iNB].n_dyes_vs_t, step_max);}
          srenew(fcs->trj[iTR].pbcbox[iNB].intensity_vs_t, step_max);
        }
      }
    }

    //Zero some reallocated variables for later "proper" use
    for (iTR=0; iTR < fcs->n_trjs; iTR++){
      for (iNB=0; iNB < fcs->pbcboxes; iNB++){
        if (bodyes) {fcs->trj[iTR].pbcbox[iNB].n_dyes_vs_t[step]=0;}
        fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[step]=0;
      }
    }

    //Save time frame in ns
    simulation_time[step]=fr.time/1000;

    //Update pbc structure for the current frame. Used by gmx_rmpbc and pbc_dx
    set_pbc(&pbc,ePBC,fr.box);

    //Calculate average box_size for statistics
    for (iD=0; iD < DIM; iD++) box_ave[iD] += fr.box[iD][iD];

    if (bRmCOMM){
      /* Adequate trajectory if user want to remove center of mass drift:
        a - calculate com from beggining
          a.1 - remove jumps respespect previous frame. (prepdata)
          a.2 - calculate com displacemnt of the current frame
          a.3 - add current displacement to acomulated
        b - Remove center mass motion for all atoms in this frame
        c - Put the atoms back to the box
        d - Make molecules whole
      */
      memcpy(xcur, fr.x, fr.natoms*sizeof(xcur[0]));
      calc_com (gnx_com[0], index_com[0], xcur, xprev, fr.box, &top.atoms, com_sel_cur);
      if (step==0) copy_rvec(com_sel_cur, com_sel_prev);
      rvec_sub(com_sel_cur, com_sel_prev, comm_sel);
      rvec_inc(comm_sel_cumulative, comm_sel);
      if (debug_level >= 3) {
        fprintf(stderr, "\nstep=%i com_prev=[%f,%f,%f]\n", step,
                             com_sel_prev[0],
                             com_sel_prev[1],
                             com_sel_prev[2]);
        fprintf(stderr, "step=%i com_cur=[%f,%f,%f]\n", step,
                             com_sel_cur[0],
                             com_sel_cur[1],
                             com_sel_cur[2]);
        fprintf(stderr, "step=%i comm=[%f,%f,%f]\n", step,
                             comm_sel[0],
                             comm_sel[1],
                             comm_sel[2]);
        fprintf(stderr, "step=%i comm_cumulative=[%f,%f,%f]\n", step,
                             comm_sel_cumulative[0],
                             comm_sel_cumulative[1],
                             comm_sel_cumulative[2]);
      }
      //Preparation of the next iteration
      copy_rvec(com_sel_cur, com_sel_prev);
      memcpy(xprev, fr.x, fr.natoms*sizeof(xprev[0]));

      //Remove center of mass motion
      for (iX=0; iX < fr.natoms; iX++) {
        fr.x[iX][0] -= comm_sel_cumulative[0];
        fr.x[iX][1] -= comm_sel_cumulative[1];
        fr.x[iX][2] -= comm_sel_cumulative[2];
      }
    }
    //Make molecules whole in multiparticle molecule simulation
    if (!bTrjCOM) gmx_rmpbc(gpbc, fr.natoms,fr.box,fr.x);

    //FIND JUMPS:
    if (n_pbc_displ !=0){
      //Find jumps & move dyes of box
      n_jumps=0;
      for (iTR=0; iTR < fcs->n_trjs; iTR++){
        for (iND=0; iND < fcs->trj[iTR].n_dyes; iND++){
          for (iD=0; iD < DIM ; iD++) {
            movement = fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][iD]
                        - fcs->trj[iTR].xyz_at0_dyes[iND][iD];
            if ( fabs(movement) > fr.box[iD][iD]/2.0 ) {
              if (debug_level >= 2) {
                fprintf (stderr,
                    "JUMP!!! step=%d(%fns) Trj=%d dye=%d box=%d coord=%d old=(%f, %f, %f) new=(%f,%f,%f)\n",
                    step, fr.time/1000, iTR, iD,
                    fcs->trj[iTR].pbcbox_dyes[iND], fcs->trj[iTR].xyz_at0_dyes[iND][0],
                    fcs->trj[iTR].xyz_at0_dyes[iND][1], fcs->trj[iTR].xyz_at0_dyes[iND][2],
                    fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][0],
                    fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][1],
                    fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][2]);
              }
              if (!bjump) n_jumps++;
              bjump=TRUE;
              jump[iD] = (movement > 0)?-1:1;
            }
            //Update positions at0 of all dyes to check jumps
            fcs->trj[iTR].xyz_at0_dyes[iND][iD] = fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]][iD];
          }
          //Store found jumps for later processing
          if (bjump) {
            if (n_jumps >= n_jumps_max){
              n_jumps_max += 50;
              srenew(jumps, n_jumps_max); //TODO:I'm not sure if this can create problems. Normally not used.
            }
            jumps[n_jumps-1][0]=iTR;
            jumps[n_jumps-1][1]=fcs->trj[iTR].pbcbox_dyes[iND];
            jumps[n_jumps-1][2]=iND;
            jumps[n_jumps-1][3]=jump[0];
            jumps[n_jumps-1][4]=jump[1];
            jumps[n_jumps-1][5]=jump[2];
            bjump=FALSE;
            for (iD=0; iD<3; iD++) jump[iD]=0;
          }
        }
      }
      //Process found jumps, moving the dyes to destination box.
      mv_dyes_due_to_jumps_between_pbcboxes (fcs, n_jumps, jumps);
      if(debug_level >= 2) {
        fprintf(stderr, "\n\n\n\n step=%d n_jumps=%d\n", step, n_jumps);
        for (iX=0; iX <n_jumps; iX++) {
          fprintf(stderr, "iTR=%d box=%d n_dye=%d jump(%d, %d, %d)\n",
                           jumps[iX][0], jumps[iX][1], jumps[iX][2],
                           jumps[iX][3], jumps[iX][4], jumps[iX][5]);
        }
      }
    } //End FIND JUMPS

    //Save number dyes in each box for output if required
    if (bodyes) {
      for (iTR=0; iTR < fcs->n_trjs; iTR++){
        for(iND=0; iND < fcs->trj[iTR].n_dyes; iND++){
          fcs->trj[iTR].pbcbox[fcs->trj[iTR].pbcbox_dyes[iND]].n_dyes_vs_t[step]++;
        }
      }
    }


    /* Calculate intensity under the beam */
    //Find position beam venter
    beam_center[0]=fr.box[XX][XX]/(float)2;
    beam_center[1]=fr.box[YY][YY]/(float)2;
    beam_center[2]=fr.box[ZZ][ZZ]/(float)2;
    if (axis_fcs != 9) beam_center[axis_fcs] = 0; //Not interested in the normal component

    for(iTR=0; (iTR < fcs->n_trjs); iTR++){
      for(iND=0; iND < fcs->trj[iTR].n_dyes; iND++){
        //Calculate center of mass of a dye
        if (bTrjCOM){
          copy_rvec(fr.x[fcs->trj[iTR].pos_atoms_dyes[iND][0]], com_dye);
        } else {
          calc_xcm(fr.x, fcs->trj[iTR].n_atoms_dyes[iND], fcs->trj[iTR].pos_atoms_dyes[iND],
                   top.atoms.atom, com_dye, FALSE);
        }
        /* TODO: check if gmx_rmpbc puts molecules inside box where COM is or just makes molecules whole.
        for(iD=0; (iD < 3); iD++) {
          if(com_dye[iD]<0) {
            fprintf( stderr, "\nNegative coordinate: com_dye[%d]=%f atom0[%d]=%f (step=%d)\n",
                            iD, com_dye[iD], iD, fcs->trj[iTR].xyz_at0_dyes[iND][iD], step);
          }
        }
        */
        //Calculation distance (coordinates) respect center box where dye is in (ORI).
        if (fcs->axis_fcs != 9) com_dye[fcs->axis_fcs] = 0; //Not interested in the normal component when 2D

        for(iD=0; iD < DIM; iD++) {
          dist_c_beam[iD] = com_dye[iD] - beam_center[iD];
          if (0){
          //if (fabs(dist_c_beam[iD])> fr.box[iD][iD]/(float)2){
             fprintf( stderr, "\n\n###############################################\n");
             fprintf( stderr, "\nDye to beam distance bigger than half box (NOT GOOD):\n");
             fprintf( stderr, "iTR=%d iND=%d pbcbox=%d step=%d\n",
                              iTR, iND, fcs->trj[iTR].pbcbox_dyes[iND],step);
             fprintf( stderr, "BOX=(%6.5f, %6.5f, %6.5f)\n",
                              fr.box[0][0], fr.box[1][1],fr.box[2][2]);
             fprintf( stderr, "xyz_com=(%6.5f, %6.5f, %6.5f)\n",
                              com_dye[0], com_dye[1], com_dye[2]);
             fprintf( stderr, "beam_center=(%6.5f, %6.5f, %6.5f)\n",
                              beam_center[0], beam_center[1], beam_center[2]);
             fprintf( stderr, "dist_c_beam=(%6.5f, %6.5f, %6.5f)\n",
                              dist_c_beam[0], dist_c_beam[1], dist_c_beam[2]);
             fprintf( stderr, "\n\n###############################################\n");

          }
        }
        //Calculation intensity contribution to every box in the trajectory
        for(iNB=0; iNB < fcs->pbcboxes; iNB++){
          for (iD=0; iD < DIM ; iD++) {
            dist_beam[iD] = dist_c_beam[iD] - ((real)xyz_displ[iNB][iD] * box[iD][iD]);
          }
          real_pbcbox = relative2real_pbcbox_M[fcs->trj[iTR].pbcbox_dyes[iND]][iNB];
          fcs->trj[iTR].pbcbox[real_pbcbox].intensity_vs_t[step] +=
                                expf(inv_focus2_extra*norm2(dist_beam));
           //                   calc_intensity_dye(fcs->type_fcs, fcs->focus, dist_beam, inv_focus2_extra);

         }
      }
    }
    /*
    fprintf(test_I2,"step=%3d pbcbox=%2d X=(%010.6f, %010.6f, %010.6f) ",
                     step,
                     fcs->trj[0].pbcbox_dyes[0],
                     com_dye[0], com_dye[1], com_dye[2]);
    for (iNB=0; (iNB < fcs->pbcboxes) ; iNB++) {
      fprintf(test_I2,"I[%d]=%3.6f ", iNB, fcs->trj[0].pbcbox[iNB].intensity_vs_t[step] );
    }
    fprintf(test_I2,"\n");
    */

    step++;
  } while(read_next_frame(oenv,status,&fr));
  //Free some memory no longer used
  if (!bTrjCOM) gmx_rmpbc_done(gpbc);


  /*****************************************************************************
  *                            ANALYSYS DATA                                   *
  *****************************************************************************/

  fprintf(olog, "\nOUTPUT DATA:\n");
  fprintf(olog, "Average size box XYZ= %f %f %f nm\n",
                  box_ave[0]/step, box_ave[1]/step, box_ave[2]/step);

  //Calculate number steps which equal to the maximum time for output
  double   time_step; //time between two frames in ns
  long     max_steps_out;
  time_step = (simulation_time[1] - simulation_time[0]); //In ns

  //Compute average intenties per box and global
  double intensity_B_ave_global; //to calculate <I>_t when operating with pbcboxes
  for( iTR=0; iTR < fcs->n_trjs; iTR++ ){
    for (iNB=0; ( iNB < fcs->pbcboxes); iNB++){
      for (iS=0; iS<step; iS++){
        fcs->trj[iTR].pbcbox[iNB].intensity_ave += fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[iS];
      }
      fcs->trj[iTR].pbcbox[iNB].intensity_ave /= (double)step;
      intensity_B_ave_global += fcs->trj[iTR].pbcbox[iNB].intensity_ave;
    }
  }
  intensity_B_ave_global = intensity_B_ave_global / ((double)fcs->n_trjs * (double)fcs->pbcboxes);
  fprintf(olog, "Average Intensity per pbcbox: %f (Using all trajectories and pbcboxes)\n",
          intensity_B_ave_global);
  //Calculation intensity time autocorrelation by FFT
  //For testing acf code
  //TODO: Add mechanism to load directly an intensity file for testing
  //Initialize fftw for acf of the intesities
  io = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * step * 2); //double size for zero padding
  plan_FT = fftw_plan_dft_1d(step*2, io, io, FFTW_FORWARD, FFTW_ESTIMATE);
  plan_iFT = fftw_plan_dft_1d(step*2, io, io, FFTW_BACKWARD, FFTW_ESTIMATE);
  for(iTR=0; iTR < fcs->n_trjs; ++iTR ){
    for (iNB=0; (iNB < fcs->pbcboxes); iNB++){
      fcs->trj[iTR].pbcbox[iNB].acf_intensity_time =  calc_acf_fftw(step,
                                        fcs->trj[iTR].pbcbox[iNB].intensity_vs_t,
                                        fcs->trj[iTR].pbcbox[iNB].intensity_ave,
                                        io, plan_FT, plan_iFT);
    }
  }
  fftw_destroy_plan(plan_FT);
  fftw_destroy_plan(plan_iFT);
  fftw_free(io);


  //Calculation of dG(0)
  double acf_intensity_zero_B=0;
  for(iTR=0; iTR < fcs->n_trjs; ++iTR ){
    for (iNB=0; (iNB < fcs->pbcboxes); iNB++){
      acf_intensity_zero_B += fcs->trj[iTR].pbcbox[iNB].acf_intensity_time[0]/
                                 pow(fcs->trj[iTR].pbcbox[iNB].intensity_ave,2);
    }
  }
  acf_intensity_zero_B /=  ( (double)fcs->n_trjs * (double)pbcboxes );
  fprintf(olog, "dG[0]= %f [Time average]=> equivalent to 1 over Number dyes under beam\n",
                 acf_intensity_zero_B);
  fprintf(olog, "N=1/dG[0]= %f [Time average]=> Number dyes under beam\n",
                 1/acf_intensity_zero_B);

  //Calculation of dG(tau)/dG(0) for each series & its average over trajectories and pbcboxes
  snew (acf_intensity_time_B_ave, step);
  for (iS= (step-1); iS >= 0; iS--){ //Here iS refers to tau
    for(iTR=0; iTR < fcs->n_trjs; ++iTR ){
      for (iNB=0; (iNB < fcs->pbcboxes); iNB++){
        fcs->trj[iTR].pbcbox[iNB].acf_intensity_time[iS] /=
                      fcs->trj[iTR].pbcbox[iNB].acf_intensity_time[0];
        acf_intensity_time_B_ave[iS] += fcs->trj[iTR].pbcbox[iNB].acf_intensity_time[iS];
      }
    }
    acf_intensity_time_B_ave[iS] /= (double)(fcs->n_trjs * fcs->pbcboxes);
  }

  if(debug_level >= 3) {
    //Calculation autocorrelation (time average) using definition for trj=0 pbcbox=0 (slow)
    // TODO: Does not plot anything right now. Maybe better to be deleted.
    // acf= <I(t)-I(0)>t
    double  *acf_intensity_time_slow;
    double  *diff_acf;
    FILE *debug_diff_acf;
    snew (acf_intensity_time_slow, step);
    snew (diff_acf, step);
    //loop over tau: As many as total number of steps.
    for (iTAU=0; iTAU < step; iTAU++){
      //loop over times
      for (iS=0; (iS < step-iTAU) ; iS++){
        acf_intensity_time_slow[iTAU] +=
           (fcs->trj[0].pbcbox[0].intensity_vs_t[iS+iTAU] - fcs->trj[0].pbcbox[0].intensity_ave) *
                     (fcs->trj[0].pbcbox[0].intensity_vs_t[iS] - fcs->trj[0].pbcbox[0].intensity_ave);
      }
      acf_intensity_time_slow[iTAU] /= (step - iTAU);
      diff_acf[0]=acf_intensity_time_slow[iTAU] - fcs->trj[0].pbcbox[0].acf_intensity_time[iTAU];
    }
    sprintf(name, "%s-debug_diff_acf.xvg", prefix);
    debug_diff_acf =  xvgropen(name,"Difference between acf using fftw and definition(slow)(trj=0 pbcbox=0)",
                            "Lag Time [ns]", "ACF(def)-ACF(fftw)", oenv);
  }

  //Calculation of the ensemble average over all trajectories and all pbc boxes
  snew (fcs->acf_intensity_ensemble, step);
  for(iTR=0; iTR < fcs->n_trjs; ++iTR ){
    for (iNB=0; (iNB < fcs->pbcboxes); iNB++){
      for (iS=0; iS<step; iS++){
        fcs->acf_intensity_ensemble[iS] +=
		(fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[iS] - intensity_B_ave_global) *
                (fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[0]  - intensity_B_ave_global);
      }
    }
  }

  fprintf(olog, "dG[0]= %f [Ensemble average]=> equivalent to 1 over Number dyes under beam\n",
                 fcs->acf_intensity_ensemble[0] /
                (fcs->n_trjs * fcs->pbcboxes * pow(intensity_B_ave_global,2)));
  fprintf(olog, "N=1/dG[0]= %f [Ensemble average]=> Number dyes under beam\n",
                 fcs->n_trjs * fcs->pbcboxes * pow(intensity_B_ave_global,2) /
                 fcs->acf_intensity_ensemble[0]);

  for (iS= (step-1); iS >= 0; iS--){ //Here iS refers to tau
    fcs->acf_intensity_ensemble[iS] /= fcs->acf_intensity_ensemble[0];
  }

  /*****************************************************************************
  *                            PLOT/WRITE OUTPUT                               *
  *****************************************************************************/

  if (bopbcbox) {
    //Time acf
    sprintf(name, "%s-B-dG.xvg", prefix);
    sprintf(xlabel,"dG(tau)/dG(0) (%d Trajectories x %d pbcboxes)", fcs->n_trjs, fcs->pbcboxes);
    oacf_B = xvgropen(name, xlabel, "Lag Time [ns]", "[Au]", oenv);
  }
  //Average Time acf
  sprintf(name, "%s-B-dG_ave.xvg", prefix);
  sprintf(xlabel,"Average dG(tau)/dG(0)(%d Trajectories x %d pbcboxes)", fcs->n_trjs, fcs->pbcboxes);
  oacf_B_ave = xvgropen(name, xlabel, "Lag Time [ns]", "[Au]", oenv);
  //Average Time acf
  sprintf(name, "%s-dG_ensemble.xvg", prefix);
  sprintf(xlabel,"Ensemble average dG(tau)/dG(0)(%d Trajectories x %d pbcboxes)", fcs->n_trjs, fcs->pbcboxes);
  oacf_ensemble = xvgropen(name, xlabel, "Lag Time [ns]", "[Au]", oenv);

  for (iS=0; iS<step; iS++){
    //Print time
     if (bopbcbox) {fprintf(oacf_B,"%6.3f",iS*time_step);} //Lag Time in ns
    fprintf(oacf_B_ave,"%6.3f",iS*time_step); //Lag Time in ns
    fprintf(oacf_ensemble,"%6.3f",iS*time_step); //Time in ns
    //Print Data
    //Global (average acf curves)
    fprintf(oacf_B_ave," %8.5f\n",acf_intensity_time_B_ave[iS]);
    fprintf(oacf_ensemble," %8.5f\n",fcs->acf_intensity_ensemble[iS]);
    if (bopbcbox) {//Per box
      for( iTR=0; iTR < fcs->n_trjs; iTR++ ){
        for (iNB=0; (iNB < fcs->pbcboxes) ; iNB++) {
          fprintf(oacf_B," %8.5f",fcs->trj[iTR].pbcbox[iNB].acf_intensity_time[iS]);
        }
      }
      fprintf(oacf_B,"\n");
    }
  }
  if (bodyes) { //Print statistics number of dyes
    //Initializations
    double  **n_dyes_B_cumulative;
    snew(n_dyes_B_cumulative, fcs->n_trjs);
    for( iTR=0; iTR < fcs->n_trjs; ++iTR ) {
      snew(n_dyes_B_cumulative[iTR],fcs->pbcboxes);
    }
    //Number of dyes
    sprintf(name, "%s-B-ndyes.xvg", prefix);
    sprintf(xlabel,"Number of dyes (%d Trajectories x %d pbcboxes)", fcs->n_trjs, fcs->pbcboxes);
    odyes_B = xvgropen(name, xlabel,"Time [ns]", "Number of dyes", oenv);
    //Average number dyes
    sprintf(name, "%s-B-ndyes_ave.xvg", prefix);
    sprintf(xlabel,"Average number of dyes (%d Trajectories x %d pbcboxes)",  fcs->n_trjs, fcs->pbcboxes);
    odyes_B_ave = xvgropen(name, xlabel, "Time [ns]", "Number of dyes", oenv);
    for (iS=0; iS<step; iS++){
      //Print time
      fprintf(odyes_B,"%6.3f",simulation_time[iS]); //In ns
      fprintf(odyes_B_ave,"%6.3f",simulation_time[iS]); //In ns
      //Print Data
      for( iTR=0; iTR < fcs->n_trjs; iTR++ ){
        for (iNB=0; (iNB < fcs->pbcboxes) ; iNB++) {
          fprintf(odyes_B," %3d",fcs->trj[iTR].pbcbox[iNB].n_dyes_vs_t[iS]);
          n_dyes_B_cumulative[iTR][iNB] += fcs->trj[iTR].pbcbox[iNB].n_dyes_vs_t[iS]; //averages
          fprintf(odyes_B_ave," %8.5f",n_dyes_B_cumulative[iTR][iNB]/ (double)(iS+1));
        }
      }
      fprintf(odyes_B,"\n");
      fprintf(odyes_B_ave,"\n");
    }
  }
  if (boint) {//Print statistics intensities
    //Initializations
    double  **intensity_B_cumulative;
    snew(intensity_B_cumulative, fcs->n_trjs);
    for( iTR=0; iTR < fcs->n_trjs; ++iTR ) {
      snew(intensity_B_cumulative[iTR],fcs->pbcboxes);
    }
    //Intensity
    sprintf(name, "%s-B-int.xvg", prefix);
    sprintf(xlabel,"Total Intensity (%d Trajectories x %d pbcboxes)",  fcs->n_trjs, fcs->pbcboxes);
    oint_B = xvgropen(name, xlabel, "Time [ns]", "Intensity [Au]", oenv);
    //Average Intensity
    sprintf(name, "%s-B-int_ave.xvg", prefix);
    sprintf(xlabel,"Average Intensity (%d Trajectories x %d pbcboxes)",  fcs->n_trjs, fcs->pbcboxes);
    oint_B_ave = xvgropen(name, xlabel, "Time [ns]", "Intensity [Au]", oenv);
    for (iS=0; iS<step; iS++){
      //Print time
      fprintf(oint_B,"%6.3f",simulation_time[iS]); //In ns
      fprintf(oint_B_ave,"%6.3f",simulation_time[iS]); //In ns
      //Print Data
      for( iTR=0; iTR < fcs->n_trjs; iTR++ ){
        for (iNB=0; (iNB < fcs->pbcboxes) ; iNB++) {
          fprintf(oint_B," %8.5f",fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[iS]);
          intensity_B_cumulative[iTR][iNB] += fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[iS]; //averages
          fprintf(oint_B_ave," %8.5f",intensity_B_cumulative[iTR][iNB]/ (double)(iS+1));
        }
      }
      fprintf(oint_B,"\n");
      fprintf(oint_B_ave,"\n");
    }
  }


  if(debug_level >= 2) { //Output intensities for checking with other programs
    FILE *test_intensity;
    test_intensity = fopen("Test_I.xvg", "w");
    for( iTR=0; iTR < fcs->n_trjs; iTR++ ){
      for( iNB=0; iNB < fcs->pbcboxes; iNB++ ){
        for (iS=0; iS<step; iS++){
          fprintf(test_intensity,"%3.6f ",fcs->trj[iTR].pbcbox[iNB].intensity_vs_t[iS] );
        }
      }
      fprintf(test_intensity,"\n");
    }
  }

  thanx(stderr);

  //gmx_fatal(FARGS, "Until Here");
  return 0;
}
